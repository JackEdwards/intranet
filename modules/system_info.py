#! /usr/bin/env python3

import datetime
import platform
import socket
import re
import subprocess
import psutil # non-standard library
import sys
from collections import namedtuple

class System:
    def __init__(self):
        # OS information
        self.platform = platform.system()
        self.release = platform.release()
        self.version = platform.version()
        self.hostname = platform.node()
        self.architecture = platform.machine()

        # Boot information
        self.boot_time = datetime.datetime.fromtimestamp(psutil.boot_time()).strftime("%Y-%m-%d %H:%M:%S")

        # Network information
        self.refresh_network_info()

        # CPU information
        if self.platform.lower() == 'windows':
            model_name = subprocess.check_output(["wmic","cpu","get", "name"]).strip().decode().split('\n')[1]
            max_cpu_mhz = psutil.cpu_freq().max
        elif self.platform.lower() == 'linux':
            lscpu = subprocess.check_output('lscpu', shell=True).strip().decode().split('\n')
            model_name = list(filter(lambda x: 'model name' in x.lower(), lscpu))[0]
            model_name = re.sub('.*model name.*: *', '', model_name, flags=re.IGNORECASE)
            max_cpu_mhz = list(filter(lambda x: 'cpu mhz' in x.lower(), lscpu))[0]
            max_cpu_mhz = re.sub('.*cpu mhz.*: *', '', cpu_mhz, flags=re.IGNORECASE)

        core_count = psutil.cpu_count(logical=True)
        max_clock = '{:.2f} {}'.format(float(max_cpu_mhz) / 1000, 'GHz')
        cinfo = namedtuple('cinfo', ['model_name', 'core_count', 'max_clock'])
        self.cpu = cinfo(model_name, core_count, max_clock)

        # Memory information
        virtual_max = '{:.2f} {}'.format(psutil.virtual_memory().total / (1024 ** 3), 'GB')
        swap_max = '{:.2f} {}'.format(psutil.swap_memory().total / (1024 ** 3), 'GB')
        mstats = namedtuple('mstats', ['max'])
        virtual = mstats(virtual_max)
        swap = mstats(swap_max)
        mtypes = namedtuple('mtypes', ['virtual', 'swap'])
        self.memory = mtypes(virtual, swap)

        # Disk partition information
        self.partitions = list()
        dpartinfo = namedtuple('dstats', ['device', 'mountpoint', 'filesystem', 'total', 'used', 'free', 'percent'])
        partitions = list(filter(lambda x: ('cdrom' not in x.opts) and ('iso9660' not in x.opts), psutil.disk_partitions(all=False)))
        for p in partitions:
            usage = psutil.disk_usage(p.device)
            self.partitions.append(dpartinfo(p.device, p.mountpoint, p.fstype, usage.total, usage.used, usage.free, usage.percent))

    # This will pause execution for "interval" seconds
    # Might want to thread this; does that even make sense?
    def get_cpu_percent(self, interval=1, percpu=True):
        return psutil.cpu_percent(interval, percpu)

    def get_memory_percent(self):
        gmtypes = namedtuple('gmtypes', ['virtual', 'swap'])
        virtual = psutil.virtual_memory().percent
        swap = psutil.swap_memory().percent
        return gmtypes(virtual, swap)

    # Theoretically this information could change after the Server() object gets created
    # Should periodically refresh it
    def refresh_network_info(self):
        self.active_ip = socket.gethostbyname(self.hostname)

        # This should be a platform agnostic way of getting the active interface and it's info
        #  Windows 10 = 'Ethernet', 'Wi-Fi'
        #  Linux 4.* -> ifconfig = 'eth0', 'wlp2s0', 'wlan0', 'lo'
        self.interface = list(dict(filter(lambda x: self.active_ip in str(x), psutil.net_if_addrs().items())).keys())[0]
        self.mac = list(filter(lambda x: 'af_link' in str(x.family).lower(), psutil.net_if_addrs()[self.interface]))[0].address

        # Windows has multiple and dynamic ipv6 addresses; not going to support ipv6 for this reason
        # self.ipv6 = list(filter(lambda x: 'AF_INET6' in str(x.family), psutil.net_if_addrs()[self.interface]))
