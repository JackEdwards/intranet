#!/usr/bin/env python3
import datetime
import os
import sys


class Logger:
    def __init__(self):
        self.config = {
            'LOG_FILE_DIRECTORY': sys.path[0],
            'LOG_FILE_NAME': 'log',
            'LOG_FILE_EXTENSION': '.txt',
            'EXCEPTION_FILE_DIRECTORY': sys.path[0],
            'EXCEPTION_FILE_NAME': 'log',
            'EXCEPTION_FILE_EXTENSION': '.txt.exc',
        }

    def log(self, severity=None, message=None, exception=None):
        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        severity_string = {
            1: 'FATAL',
            2: 'ERROR',
            3: 'INFO',
            4: 'DEBUG',
            5: 'TRACE'
        }

        if severity not in severity_string:
            severity_string[severity] = 'INVALID'

        if message:
            try:
                with open(self.log_file_path, 'a') as f:
                    s = '{} | {} | {}\n'.format(now, severity_string[severity], message)
                    f.write(s)
            except Exception:  # Don't have a reason to handle different exceptions differently right now
                print('Unhandled exception occurred: {}'.format(sys.exc_info()))

        if exception:
            try:
                with open(self.exc_file_path, 'a') as f:
                    s = '{} | {} | {}\n'.format(now, severity_string[severity], exception)
                    f.write(s)
            except Exception:  # Don't have a reason to handle different exceptions differently right now
                print('Unhandled exception occurred: {}'.format(sys.exc_info()))

        return True

    @property
    def log_file_path(self):
        return '{}{}{}{}'.format(
            self.config['LOG_FILE_DIRECTORY'],
            os.sep,
            self.config['LOG_FILE_NAME'],
            self.config['LOG_FILE_EXTENSION']
        )

    @property
    def exc_file_path(self):
        return '{}{}{}{}'.format(
            self.config['EXCEPTION_FILE_DIRECTORY'],
            os.sep,
            self.config['EXCEPTION_FILE_NAME'],
            self.config['EXCEPTION_FILE_EXTENSION']
        )
