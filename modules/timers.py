from threading import Timer

import intranet.library.database
import functions.server


# Example code.  Custom functions no longer exist, but Timer is good.
def timer_cpu_mem():
	Timer(5.0, timer_cpu_mem).start()
	session = intranet.library.database.Session()
	cpu = str(functions.server.get_cpu())
	virt = str(functions.server.get_virtual_mem())
	swap = str(functions.server.get_swap_mem())
	cpu_mem = intranet.library.database.CPU_MEM(cpu=cpu, virt=virt, swap=swap)
	session.add(cpu_mem)
	session.commit()
