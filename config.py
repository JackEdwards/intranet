import configparser
import os
import uuid

# Build the path to the config file.
script_path = os.path.dirname(__file__)
config_path = os.path.join(script_path, 'config.ini')

# Create the config file if it does not exist.
open_file = os.open(config_path, os.O_CREAT)
os.close(open_file)

# Create the 'config' object.
config = configparser.ConfigParser()

# Verify sections in the config.ini file.
# If sections do not exist in the proper order, reset the file to default values.
config.read(config_path)
if config.sections() != ['Flask', 'Request Headers']:
    config['Flask'] = {
        'Server Name': 'intranet.local',
        'Debug': 'False',  # Debug will restart the app upon startup (when using Werkzeug)
        'Secret Key': '9',
        'Pepper': uuid.uuid4().hex}

    config['Request Headers'] = {
        'Access-Control-Allow-Origin': 'http://intranet.local',
        'Access-Control-Allow-Credentials': 'False',
    }

    os.remove(config_path)
    with open(config_path, 'w') as config_file:
        config.write(config_file)
    config.read(config_path)

# Config file is valid and in memory.
