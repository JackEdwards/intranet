from setuptools import setup

setup(
    name='Pi-Dashboard',
    version='0.1',
    # 'psutil' may require python3-dev
    install_requires=['Flask', 'flask-socketio', 'sqlalchemy', 'psutil'],
    url='',
    license='',
    author='Jack Edwards',
    author_email='jackeedwardsjr@gmail.com',
    description='An intranet site.'
)
