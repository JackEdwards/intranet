if __name__ == '__main__':
    import intranet
    from intranet.library import database

    # Generate an Admin user, for initial login
    session = database.Session()
    try:
        admin_user = database.method.user.generate_admin_user()
        session.commit()
    except:
        session.rollback()
        raise
    print(' * Admin credentials: {username} / {password}'.format(username=admin_user[0], password=admin_user[1]))

    # Run the app
    intranet.socketio.run(intranet.app, host='127.0.0.1', port=80)
