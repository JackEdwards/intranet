#!/usr/bin/env python3

import flask
import flask_socketio  # ToDo - install Eventlet (http://eventlet.net/)
import config

#  Config
app = flask.Flask(__name__)

app.config['SERVER_NAME'] = config.config['Flask']['Server Name']
app.config['DEBUG'] = config.config['Flask'].getboolean('Debug')
app.secret_key = config.config['Flask']['Secret Key']
pepper = config.config['Flask']['Pepper']
socketio = flask_socketio.SocketIO(app)


def add_cors_headers(response):
    response.headers['Access-Control-Allow-Origin'] = 'http://intranet.dev'
    # response.headers['Access-Control-Allow-Credentials'] = 'true'
    return response


app.after_request(add_cors_headers)

#  Website
import intranet.routes

#  Database
import intranet.library.database

