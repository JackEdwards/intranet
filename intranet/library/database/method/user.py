import base64
import os
import re
import hashlib
import uuid
import inspect
from datetime import datetime

from intranet import pepper
from intranet.library.database.model import User
from intranet.library import utility
from intranet.library import database


def create_user(first, last, email, username, password, group, image):
    if utility.regex.username_regex_match(username):
        if utility.regex.password_regex_match(password):
            session = database.Session()
            if not user_exists(username):
                salt = uuid.uuid4().hex
                user = User(id=salt,
                            first=first,
                            last=last,
                            email=email,
                            username=username,
                            password=hashlib.sha256(str.encode(password + salt + pepper)).hexdigest(),
                            group=group,
                            image=image)
                session.add(user)
                return True
    return False


def update_user(first, last, email, username, old_password, new_password, group, image):
    current_frame = inspect.currentframe()
    calling_frame = inspect.getouterframes(current_frame, 2)
    admin_method = 'generate_admin_user'

    if calling_frame[1][1] == __file__ and calling_frame[1][3] == admin_method:
        pass
    elif authenticate_user(username, old_password):
        pass
    else:
        return False

    # User authenticated.  Update the user record.
    session = database.Session()
    user = session.query(User).filter(User.username.ilike(username)).first()
    salt = user.id
    user.first = first
    user.last = last
    user.email = email
    user.username = username
    user.password = hashlib.sha256(str.encode(new_password + salt + pepper)).hexdigest()
    user.group = group
    user.image = image
    user.modified = datetime.utcnow()
    return True


# Generate a new password for the admin user every time the app starts
def generate_admin_user():
    random_bytes = os.urandom(8)
    decoded = base64.b64encode(random_bytes).decode('utf-8')
    pattern = re.compile('[\W_]+')
    gen_password = pattern.sub('', decoded)

    session = database.Session()
    count = session.query(User).filter(User.username.ilike('admin')).count()
    if count == 0:
        # Admin user does not exist.  Create one.
        create_user('Admin', None, None, 'Admin', gen_password, 'Administrator', None)
        user = session.query(User).filter(User.username.ilike('admin')).first()
    else:
        # Admin user already exists.  Update the password with the newly generated password.
        user = session.query(User).filter(User.username.ilike('admin')).first()
        update_user('Admin', None, None, 'Admin', None, gen_password, 'Administrator', None)
    return user.username, gen_password


# This gets used during Login
def authenticate_user(username, password):
    session = database.Session()
    user = session.query(User).filter(User.username.ilike(username)).first()
    if user:
        # User found in the table
        salt = user.id
        hashed_password = hashlib.sha256(str.encode(password + salt + pepper)).hexdigest()
        if user.password == hashed_password:
            # Password matches.  Request is authentic.
            return True
    # The request is bad or not authentic.
    return None


def get_user_list():
    users_list = list()
    session = database.Session()
    user_query = session.query(User).all()

    # Why is this enumerating?
    for index, user in enumerate(user_query):
        user_dict = dict()
        user_dict['Username'] = user.username
        user_dict['First'] = user.first
        user_dict['Last'] = user.last
        user_dict['Email'] = user.email
        user_dict['Group'] = user.group
        user_dict['Modified'] = user.modified
        user_dict['Created'] = user.created

        # Replace None with empty string
        for key, value in user_dict.items():
            if value is None:
                user_dict[key] = ''
        users_list.append(user_dict)
    return users_list


def user_exists(username):
    session = database.Session()
    count = session.query(User).filter(User.username.ilike(username)).count()
    if count == 0:
        # print('functions.database | user_exists | User does not exist')
        return False
    else:
        # print('functions.database | user_exists | User already exists')
        return True


def get_group_for_user(username):
    session = database.Session()
    user = session.query(User).filter(User.username.ilike(username)).first()
    if user:
        return user.group
    return False
