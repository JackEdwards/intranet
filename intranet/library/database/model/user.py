import uuid
from datetime import datetime

from sqlalchemy import Column
from sqlalchemy import String
from intranet.library.database import Base


# Create the 'user' table
class User(Base):
    __tablename__ = 'user'

    id = Column(String, primary_key=True, default=uuid.uuid4())
    first = Column(String)
    last = Column(String)
    email = Column(String)
    username = Column(String)
    password = Column(String)
    group = Column(String)
    image = Column(String)
    modified = Column(String, default=str(datetime.utcnow()))
    created = Column(String, default=str(datetime.utcnow()))
