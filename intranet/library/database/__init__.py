import os

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session

# Build the path to the database file.
app_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
database_path = os.path.join(app_path, 'data')
if not os.path.exists(database_path):
    os.makedirs(database_path)

# http://docs.sqlalchemy.org/en/rel_1_0/orm/tutorial.html
Engine = create_engine('{prefix}:///{path}/{file}'.format(
    prefix='sqlite', path=database_path, file='main.db'), echo=False)

# Start some voodoo
Base = declarative_base()
Session_Factory = sessionmaker(bind=Engine)
Session = scoped_session(Session_Factory)

# Import object models
from intranet.library.database import model

# Import object methods
from intranet.library.database import method

# Finish the voodoo
Base.metadata.create_all(bind=Engine, checkfirst=True)
