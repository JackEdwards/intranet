import re

from intranet.library import database


def username_regex_match(username):
    # Check username against regex
    username_pattern = re.compile('^(?=.{3,})[a-zA-Z0-9]+$')
    username_match = username_pattern.match(username)
    if username_match:
        return True
    else:
        return False


def password_regex_match(password):
    # Check password against regex
    password_pattern = re.compile('^(?=.{8,})[a-zA-Z0-9@!#$%&?]+$')
    password_match = password_pattern.match(password)
    if password_match:
        return True
    else:
        return False


def name_regex_match(name):
    # Check a first or last name against regex
    name_pattern = re.compile('^(?=.{3,})[a-zA-Z-]+$')
    name_match = name_pattern.match(name)
    if name_match:
        return True
    else:
        return False


def email_regex_match(email):
    # Check an email against regex
    email_pattern = re.compile('^([a-zA-Z0-9_-]+[\.])?[a-zA-Z0-9_-]+@([a-zA-Z0-9_-]{2,12})+[\.]([a-zA-Z0-9]{2,8})+$')
    email_match = email_pattern.match(email)
    if email_match:
        return True
    else:
        return False
