import flask

from modules import system_info
from intranet import app
from intranet.library import database
from intranet.library import utility


@app.route('/', subdomain='service')
def service_index():
    return 'Hello world!'


@app.route('/login', subdomain='service', methods=['POST'])
def service_login():
    if flask.request.method == 'POST':
        session = database.Session()
        try:
            #  Validate the user
            if database.method.user.authenticate_user(flask.request.form['username'],
                                                      flask.request.form['password']):
                flask.session['username'] = flask.request.form['username']
                flask.session['group'] = database.method.user.get_group_for_user(flask.request.form['username'])
                session.commit()
                if flask.session['group'] == 'Pending':
                    flask.session.pop('username', None)
                    flask.session.pop('group', None)
                    return flask.redirect(flask.url_for('index', login_error='Access not yet granted'))
                else:
                    return flask.redirect(flask.url_for('index', login_error=None))
            else:
                return flask.redirect(flask.url_for('index', login_error='Invalid username or password'))
        except:
            session.rollback()
            raise
        finally:
            session.close()


@app.route('/logout', subdomain='service', methods=['POST'])
def service_logout():
    if flask.request.method == 'POST':
        flask.session.pop('username', None)
        flask.session.pop('group', None)
        return flask.redirect(flask.url_for('index', login_error=None))


#  Anonymous visitor creates a record in the 'user' table
#  The new user record will not have login capability until approved by an administrator
@app.route('/request-access', subdomain='service', methods=['POST'])
def service_request_access():
    if flask.request.method == 'POST':

        # Args
        first = flask.request.form['first']
        last = flask.request.form['last']
        email = flask.request.form['email']
        username = flask.request.form['username']
        password = flask.request.form['password']
        confirm = flask.request.form['confirm']

        # Return an error message if any argument is missing
        if (first is None
                or last is None
                or email is None
                or username is None
                or password is None
                or confirm is None):
            request_error = 'Missing argument(s)'
            return flask.redirect(flask.url_for('page_request_access', request_error=request_error))

        # Return an error message if any argument does not match regex
        if not utility.regex.name_regex_match(first) or\
                not utility.regex.name_regex_match(last) or\
                not utility.regex.username_regex_match(username) or\
                not utility.regex.password_regex_match(password) or\
                not utility.regex.email_regex_match(email):
            request_error = 'Invalid argument(s)'
            return flask.redirect(flask.url_for('page_request_access', request_error=request_error))

        # Return an error if the username is already in use
        session = database.Session()
        try:
            if database.method.user.user_exists(username):
                # Tell the user something is wrong with the username...
                # ... but don't explicitly tell them the username is already in use
                request_error = 'Invalid username'
                session.commit()
                return flask.redirect(flask.url_for('page_request_access', request_error=request_error))

            # No errors occurred \o/
            database.method.user.create_user(first, last, email, username, password, 'Pending', None)
            session.commit()
            dialog = 'Success: The request was received'
            return flask.redirect(flask.url_for('page_request_access', dialog=dialog))
        except:
            session.rollback()
            raise
        finally:
            session.close()


@app.route('/get-system-info', subdomain='service', methods=['GET'])
def service_get_system_info():
    cpu = list()
    i = 0
    for value in sysinfo.get_cpu_percent():
        core = dict()
        core['core'] = i
        core['percent'] = value
        cpu.append(core)
        i += 1
    memory = sysinfo.get_memory_percent()
    return flask.jsonify({'cpu': cpu, 'memory': memory})


@app.route('/approve-user', subdomain='service', methods=['POST'])
def service_approve_user():
    if 'username' in flask.session:
        if flask.session['group'] == 'Administrator':
            approved_user = flask.request.args.get('approved_user')
            new_group = flask.request.args.get('new_group')


# Authenticated user creates a record in the 'user' table
# @app.route('/create-user', subdomain='service', methods=['POST'])
# def service_create_user():
#
#     # There must be an active session
#     if 'username' in flask.session:
#         if flask.request.method == 'POST':
#             result = database.method.user.create_user(flask.request.form['username'], flask.request.form['password'])
#             if result:
#                 dialog = 'Success: User created'
#             else:
#                 dialog = 'Error: User not created'
#             return flask.redirect(flask.url_for('page_users', dialog=dialog))
#     else:
#         return flask.redirect(flask.url_for('index', login_error='Session not found'))

# User can check to see if the given username is okay to use (for a new user)
# Use-case would be an AJAX request; the user can fill in the form, press 'Validate' button, and not have
#    to re-enter the entire form again if the username is invalid
# @app.route('/validate-new-user', subdomain='service', methods=['GET'])
# def service_validate_new_user():
#
#     # There must be an active session
#     if 'username' in flask.session:
#         if flask.request.method == 'GET':
#             username = flask.request.args.get('username')
#             password = flask.request.args.get('password')
#
#             valid_username = functions.user.new_username_validation(username)
#             if not valid_username:
#                 return flask.jsonify({'error': 0})
#
#             valid_password = functions.user.new_password_validation(password)
#             if not valid_password:
#                 return flask.jsonify({'error': 1})
#
#             return flask.jsonify({'success': 0})
#     else:
#         return flask.redirect(flask.url_for('index', login_error='Session not found'))
