import flask

import modules.system_info
from intranet.library import database
from intranet import app


@app.route('/')
def index():
    if 'username' in flask.session:
        return flask.redirect(flask.url_for('page_dashboard'))
    else:
        login_service = 'http://service.intranet.local/login'
        login_error = flask.request.args.get('login_error')
        return flask.render_template('page/login.html', title='Login', loginError=login_error, loginService = login_service)


@app.route('/request-access')
def page_request_access():
    dialog = flask.request.args.get('dialog')
    request_error = flask.request.args.get('request_error')
    return flask.render_template('page/request_access.html',
                                 title='Request Access',
                                 dialog=dialog,
                                 requestError=request_error)


@app.route('/dashboard')
def page_dashboard():
    if 'username' in flask.session:
        user = {'username': flask.session['username'], 'group': flask.session['group']}
        return flask.render_template('page/dashboard.html', title='Dashboard', user=user, active='Dashboard')
    else:
        return flask.redirect(flask.url_for('index', login_error='Session not found'))


@app.route('/system')
def page_system():
    if 'username' in flask.session:
        user = {'username': flask.session['username'], 'group': flask.session['group']}
        system = modules.system_info.System()
        #basic_info = functions.server.get_basic_info()
        #cpu_info = functions.server.get_cpu_info()
        #memory_info = functions.server.get_memory_info()
        #partition_info = functions.server.get_partition_info()
        #server_info = {'name': basic_info['Name'],
        #               'os': basic_info['OS'][0],
        #               'version': basic_info['OS'][1],
        #               'release': basic_info['Release'],
        #               'ipv4': basic_info['IPv4'],
        #               'ipv4_type': basic_info['IPv4_Type'],
        #               'ipv6': basic_info['IPv6'],
        #               'ipv6_type': basic_info['IPv6_Type'],
        #               'mac': basic_info['MAC'],
        #               'mac_type': basic_info['MAC_Type'],
        #               'cpu_model': cpu_info['Model'],
        #               'cpu_max': cpu_info['Max'],
        #               'cpu_architecture': cpu_info['Architecture'],
        #               'cpu_cores': cpu_info['Cores'],
        #               'memory_virtual': memory_info['Virtual'],
        #               'memory_swap': memory_info['Swap'],
        #               'partitions': partition_info,
        #               'cores': functions.server.get_cpu_percent(),
        #               }
        return flask.render_template('page/system.html', title='System', user=user, system=system, active='System')
    else:
        return flask.redirect(flask.url_for('index', login_error='Session not found'))


@app.route('/users')
def page_users():
    if 'username' in flask.session:
        user = {'username': flask.session['username'], 'group': flask.session['group']}
        session = database.Session()
        try:
            user_list = database.method.user.get_user_list()
            session.commit()
        except:
            session.rollback()
            raise
        finally:
            session.close()
        dialog = flask.request.args.get('dialog')
        return flask.render_template('page/users.html',
                                     title='Users',
                                     user=user,
                                     active='Users',
                                     dialog=dialog,
                                     users=user_list)
    else:
        return flask.redirect(flask.url_for('index', login_error='Session not found'))


@app.route('/settings')
def page_settings():
    if 'username' in flask.session:
        user = {'username': flask.session['username'], 'group': flask.session['group']}
        page = {'dashboard': 'inactive', 'server': 'inactive', 'users': 'inactive', 'settings': 'active'}
        return flask.render_template('page/settings.html', title='Settings', user=user, active='Settings')
    else:
        return flask.redirect(flask.url_for('index', login_error='Session not found'))


@app.route('/test')
def test():
    return flask.render_template('page/test.html')
