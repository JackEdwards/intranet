function doRegexMatch(inputElement, regexPattern) {
    var regexMatch = inputElement.value.match(regexPattern);
    if (regexMatch != null) {
        return true;
    } else {
        return false;
    }
}

function updateValidation(validationElement, isValid, title) {
    if (isValid == true) {
        validationElement.innerHTML = "&#x2714";
        validationElement.classList.add("green");
        validationElement.classList.remove("red");
        validationElement.title = title;
    } else {
        validationElement.innerHTML = "&#x2718";
        validationElement.classList.add("red");
        validationElement.classList.remove("green");
        validationElement.title = title;
    }
}

function doPasswordsMatch(inputPassword, inputConfirm) {

    if (inputPassword.length == 0) {
        return false;
    }

    if (inputPassword.value == inputConfirm.value) {
        return true;
    } else {
        return false;
    }
}

function updateSubmit(validationElements, submitButton) {

    var anyRed = false;

    // Check the status of each validation element
    for (var i = 0; i < validationElements.length; i++) {
        if (!(validationElements[i].classList.contains("green"))) {
            anyRed = true;
            break;
        }
    }

    if (anyRed) {
        // One or more validation elements are red
        submitButton.disabled = true;
        submitButton.classList.remove("button-hover");
    } else {
        // None of the validation elements are red
        submitButton.disabled = false;
        submitButton.classList.add("button-hover");
    }
}

document.addEventListener("DOMContentLoaded", function() {
    console.log("Users.js:  Document ready");

    // Put all the "validation" elements in this array (if they exist)
    var validationElements = [];

    // These exist in request_access.html and users.html
    var inputUsername = document.getElementById("username");
    var inputPassword = document.getElementById("password");
    var inputConfirm = document.getElementById("confirm");
    var inputUsernameValidation = document.getElementById("username-validation");
    var inputPasswordValidation = document.getElementById("password-validation");
    var inputConfirmValidation = document.getElementById("confirm-validation");

    // Regex is the same for both pages
    var usernameRegex = /^(?=.{3,})[a-zA-Z0-9]+$/;
    var passwordRegex = /^(?=.{8,})[a-zA-Z0-9@!#$%&?]+$/;

    // These only exist in request_access.html
    var inputFirst = document.getElementById("first");
    var inputLast = document.getElementById("last");
    var inputEmail = document.getElementById("email");
    var inputFirstValidation = document.getElementById("first-validation");
    var inputLastValidation = document.getElementById("last-validation");
    var inputEmailValidation = document.getElementById("email-validation");

    // Regex only used in request_access.html
    var nameRegex = /^(?=.{3,})[a-zA-Z-]+$/;
    var emailRegex = /^([a-zA-Z0-9_-]+[\.])?[a-zA-Z0-9_-]+@([a-zA-Z0-9_-]{2,12})+[\.]([a-zA-Z0-9]{2,8})+$/;

    // This only exists in users.html
    var createUserSubmitButton = document.getElementById("create-user-submit-button");

    // This only exists in request_access.html
    var requestAccessSubmitButton = document.getElementById("request-access-submit-button");

    if (createUserSubmitButton) {
        var submitButton = createUserSubmitButton;
    }

    if (requestAccessSubmitButton) {
        var submitButton = requestAccessSubmitButton;
    }

    if (inputUsernameValidation) {
        validationElements.push(inputUsernameValidation);
    }

    if (inputPasswordValidation) {
        validationElements.push(inputPasswordValidation);
    }

    if (inputConfirmValidation) {
        validationElements.push(inputConfirmValidation);
    }

    if (inputFirstValidation) {
        validationElements.push(inputFirstValidation);
    }

    if (inputLastValidation) {
        validationElements.push(inputLastValidation);
    }

    if (inputEmailValidation) {
        validationElements.push(inputEmailValidation);
    }

    if (inputFirst) {
        inputFirst.addEventListener("input", function() {
            var regexMatch = doRegexMatch(inputFirst, nameRegex);

            if (regexMatch == true) {
                updateValidation(inputFirstValidation, true, "");
            } else {
                if (inputFirst.value.length < 3) {
                    updateValidation(inputFirstValidation, false, "Too short");
                } else {
                    updateValidation(inputFirstValidation, false, "Invalid characters");
                }
            }
            // Update the state of the submit button
            updateSubmit(validationElements, submitButton);
        });
    }

    if (inputLast) {
        inputLast.addEventListener("input", function() {
            var regexMatch = doRegexMatch(inputLast, nameRegex);

            if (regexMatch == true) {
                updateValidation(inputLastValidation, true, "");
            } else {
                if (inputLast.value.length < 3) {
                    updateValidation(inputLastValidation, false, "Too short");
                } else {
                    updateValidation(inputLastValidation, false, "Invalid characters");
                }
            }
            // Update the state of the submit button
            updateSubmit(validationElements, submitButton);
        });
    }

    if (inputEmail) {
        inputEmail.addEventListener("input", function() {
            var regexMatch = doRegexMatch(inputEmail, emailRegex);

            if (regexMatch == true) {
                updateValidation(inputEmailValidation, true, "");
            } else {
                if (inputEmail.value.length < 8) {
                    updateValidation(inputEmailValidation, false, "Too short");
                } else {
                    updateValidation(inputEmailValidation, false, "Invalid email");
                }
            }
            // Update the state of the submit button
            updateSubmit(validationElements, submitButton);
        });
    }

    if (inputUsername) {
        inputUsername.addEventListener("input", function() {
            var regexMatch = doRegexMatch(inputUsername, usernameRegex);

            if (regexMatch == true) {
                updateValidation(inputUsernameValidation, true, "");
            } else {
                if (inputUsername.value.length < 3) {
                    updateValidation(inputUsernameValidation, false, "Too short");
                } else {
                    updateValidation(inputUsernameValidation, false, "Invalid characters");
                }
            }
            // Update the state of the submit button
            updateSubmit(validationElements, submitButton);
        });
    }

    if (inputPassword) {
        inputPassword.addEventListener("input", function() {
            var regexMatch = doRegexMatch(inputPassword, passwordRegex);
            var passwordsMatch = doPasswordsMatch(inputPassword, inputConfirm);

            if (regexMatch == true) {
                updateValidation(inputPasswordValidation, true, "");
                if (passwordsMatch == true) {
                    updateValidation(inputConfirmValidation, true, "");
                } else {
                    updateValidation(inputConfirmValidation, false, "Passwords do not match");
                }
            } else {
                updateValidation(inputConfirmValidation, false, "Invalid password");
                if (inputPassword.value.length < 8) {
                    updateValidation(inputPasswordValidation, false, "Too short");
                } else {
                    updateValidation(inputPasswordValidation, false, "Invalid characters");
                }
            }

            // Update the state of the submit button
            updateSubmit(validationElements, submitButton);
        });
    }

    if (inputConfirm) {
        inputConfirm.addEventListener("input", function() {
            var passwordsMatch = doPasswordsMatch(inputPassword, inputConfirm);
            var regexMatch = doRegexMatch(inputConfirm, passwordRegex);

            if (passwordsMatch == true && regexMatch == true) {
                updateValidation(inputConfirmValidation, true, "");
            } else {
                if (passwordsMatch != true) {
                    updateValidation(inputConfirmValidation, false, "Passwords do not match");
                } else {
                    updateValidation(inputConfirmValidation, false, "Invalid password");
                }
            }
            // Update the state of the submit button
            updateSubmit(validationElements, submitButton);
        });
    }
});