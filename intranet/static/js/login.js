document.addEventListener("DOMContentLoaded", function() {
    console.log("Login.js:  Document ready");

    var loginError = document.getElementById("login-error");
    if (loginError != null) {
        window.setTimeout(function() {
            loginError.classList.toggle('slide-up');
        }, 4000);
    }
});