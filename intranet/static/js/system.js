//////
//
//  GetPerformance() gets updated system info
//    - CPU (each core as a percentage)
//    - Memory (each type as a two integers; InUse and Total)
//  https://developer.mozilla.org/en-US/docs/AJAX/Getting_Started
//
//////
function GetPerformance() {

    //  GET the number of cores from API
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            // everything is good, the response is received
            if (httpRequest.status === 200) {
                var json = httpRequest.responseText;
                var jsonArray = JSON.parse(json);

                //  (CPU)
                var parentTd = document.getElementById("cpu-polyline-td");
                var childCount = parentTd.childElementCount;
                console.log(childCount);

                //  If the Polylines do not have points, add points
                //  This needs to be done in JS, due to dynamic SVG widths
                for (var i = 0; i < childCount; i++) {
                    var polyline = document.getElementById("polyline-" + i);
                    var points = polyline.getAttributeNS(null, "points");
                    if (points == "") {
                        AddBasePoints(polyline.parentNode, polyline, 20);
                    }
                }

                for (var x = 0; x < childCount; x++) {
                    var divElement = document.getElementById("core-" + x);
                    var svgElement = divElement.getElementsByTagNameNS("http://www.w3.org/2000/svg", "svg")[0];
                    UpdateSVG(svgElement, jsonArray.cpu[x]);
                }

                //  (Virtual Memory) The child divs should already exist in the DOM
                var memoryVirtualParentCell = document.getElementById('memory-virtual-td');
                var memoryVirtualInnerCells = memoryVirtualParentCell.getElementsByClassName('memory-inner');
                for (var x = 0; x < memoryVirtualInnerCells.length; x++) {
                    memoryVirtualInnerCells[x].style = "width: " + jsonArray.memory.virtual + "%";
                }

                //  (Swap Memory) The child divs should already exist in the DOM
                var memorySwapParentCell = document.getElementById('memory-swap-td');
                var memorySwapInnerCells = memorySwapParentCell.getElementsByClassName('memory-inner');
                for (var x = 0; x < memorySwapInnerCells.length; x++) {
                    memorySwapInnerCells[x].style = "width: " + jsonArray.memory.swap + "%";
                }

            } else {
                // there was a problem with the request,
                // for example the response may contain a 404 (Not Found)
                // or 500 (Internal Server Error) response code
            }
        } else {
            // still not ready
        }
    };
    // httpRequest.timeout = 1000;
    httpRequest.open("GET", "http://service.intranet.local/get-system-info", true);
    httpRequest.send();

}

function AddBasePoints(parentElement, polyline, pointCount) {
    var rect = parentElement.getBoundingClientRect();
    var chartWidth = rect.width;
    var chartHeight = rect.height;

    var points = "0," + chartHeight;
    for (var x = 1; x <= pointCount; x++) {
        points += " " + (chartWidth / pointCount * x) + "," + chartHeight;
    }
    polyline.setAttributeNS(null, "points", points);
}

function UpdateSVG(svgElement, cpuData) {
    var polylineElement = svgElement.getElementsByTagNameNS("http://www.w3.org/2000/svg", "polyline")[0];
    var polylinePoints = polylineElement.getAttributeNS(null, "points").split(" ");

    var svgRect = svgElement.getBoundingClientRect();
    var svgWidth = svgRect.width;
    var svgHeight = svgRect.height;

    if (polylinePoints.length >= 20) {
        polylinePoints.shift();
        for (var i = 0; i < polylinePoints.length; i++) {
            var pair = polylinePoints[i].split(",");
            pair[0] = i * (svgWidth / 20);
            polylinePoints[i] = pair[0] + "," + pair[1];
        }
    }
    polylinePoints.push(svgWidth + "," + (svgHeight - cpuData.percent));
    var newPoints = polylinePoints[0];
    for (var i = 1; i < polylinePoints.length; i++) {
        newPoints = newPoints + " " + polylinePoints[i];
    }
    newPoints.trim();
    polylineElement.setAttributeNS(null, "points", newPoints);
}

//if (key == "memory") {
  //  $.each(value, function(key, value) {
    //    console.log(value.type + ': ' + value.percent);
      //  var bar = $('.' + value.type + '-mem-usage');
        //bar.width(value.percent + '%');
    //});
//}

document.addEventListener("DOMContentLoaded", function() {
    console.log("System.js:  Document ready");

    GetPerformance();
    var x = setInterval(GetPerformance, 5000);
});